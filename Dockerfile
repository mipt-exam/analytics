FROM openjdk:11
COPY ./target/analytics.jar /app/
WORKDIR /app/
EXPOSE 49082
CMD java -jar /app/analytics.jar