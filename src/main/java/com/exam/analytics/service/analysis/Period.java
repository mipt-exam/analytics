package com.exam.analytics.service.analysis;

public enum Period {
    MINUTE(1),
    FIVE_MINUTES(5),
    FIFTEEN_MINUTES(15),
    HALF_AN_HOUR(30),
    HOUR(60);

    final long inMinutes;

    Period(long inMinutes) {
        this.inMinutes = inMinutes;
    }
}
