package com.exam.analytics.service.analysis;

import com.exam.analytics.storage.OHLC;
import com.exam.analytics.web.dto.CurrencyValueDTO;

import java.util.ArrayList;
import java.util.Map;

public interface ValueAnalytic {

    Map<String, OHLC> analyticValue(Period period, ArrayList<CurrencyValueDTO> list);
}
