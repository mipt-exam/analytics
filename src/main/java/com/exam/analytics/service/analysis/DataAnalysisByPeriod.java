package com.exam.analytics.service.analysis;

import com.exam.analytics.storage.OHLC;
import com.exam.analytics.web.dto.CurrencyValueDTO;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.Map;

@Component
public class DataAnalysisByPeriod implements ValueAnalytic{

    @Override
    public Map<String, OHLC> analyticValue(Period period, ArrayList<CurrencyValueDTO> list) {

        Map<String, OHLC> dataset = new HashMap<>();
        OHLC ohlc = null;

        LocalDateTime lastProcessedDate = null;
        StringBuilder key = new StringBuilder();

        for (CurrencyValueDTO c : list) {
            if (lastProcessedDate == null ||
                    c.getMoment().isAfter(lastProcessedDate.plusMinutes(period.inMinutes)) ||
                    c.getMoment().isEqual(lastProcessedDate.plusMinutes(period.inMinutes))) {

                if (!(lastProcessedDate == null)) {
                    key.append(" - ").append(lastProcessedDate.plusMinutes(period.inMinutes).format(DateTimeFormatter.ISO_TIME));
                    //System.out.println("    period close: open – " + ohlc.open() + ", high: – " + ohlc.high() + ", low – " + ohlc.low() + ", close – " + ohlc.close());
                    dataset.put(key.toString(), ohlc);
                }

                lastProcessedDate = c.getMoment()
                        .minusNanos(c.getMoment().getNano())
                        .minusSeconds(c.getMoment().getSecond())
                        .minusMinutes(c.getMoment().getMinute() % period.inMinutes);

                //System.out.println("    new period has become: " + lastProcessedDate.format(DateTimeFormatter.ISO_TIME));
                key = new StringBuilder(lastProcessedDate.format(DateTimeFormatter.ISO_TIME));
                ohlc = new OHLC(c.getValue(), c.getValue(), c.getValue(), c.getValue());
            }

            if (c.getValue().compareTo(ohlc.high()) > 0) {
                ohlc.high(c.getValue());
            }

            if (c.getValue().compareTo(ohlc.low()) < 0) {
                ohlc.low(c.getValue());
            }

            ohlc.close(c.getValue());

            //System.out.println(c.getMoment().format(DateTimeFormatter.ISO_DATE_TIME) + ": " + c.getValue());
        }

        key.append(" - ").append(lastProcessedDate.plusMinutes(period.inMinutes).format(DateTimeFormatter.ISO_TIME));
        //System.out.println("    period close: open – " + ohlc.open() + ", high: – " + ohlc.high() + ", low – " + ohlc.low() + ", close – " + ohlc.close());
        dataset.put(key.toString(), ohlc);

        return dataset;
    }
}
