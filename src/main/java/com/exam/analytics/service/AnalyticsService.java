package com.exam.analytics.service;

import com.exam.analytics.service.analysis.DataAnalysisByPeriod;
import com.exam.analytics.service.analysis.Period;
import com.exam.analytics.storage.OHLC;
import com.exam.analytics.web.dto.CurrencyValueDTO;
import com.exam.analytics.storage.DataStorage;
import com.exam.analytics.web.dto.FullCurValueDTO;
import com.exam.analytics.web.JournalFeignClient;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.*;

@Service
public class AnalyticsService {
    private static final Logger logger = LogManager.getLogger(AnalyticsService.class);

    private final JournalFeignClient client;
    private final DataStorage dataStorage;
    private Map<String, OHLC> finalStorage = new HashMap<>();
    private List<String> currencyListFromDataBase = new ArrayList<>();
    private List<FullCurValueDTO> valueListFromDataBase = new ArrayList<>();

    private boolean isAnalyticsRunning = false;

    @Autowired
    public AnalyticsService(DataStorage dataStorage, JournalFeignClient client) {
        this.client = client;
        this.dataStorage = dataStorage;

        getCurrencyListFromDataBase();
        getValueFromJournal();

        start();
    }

    public void getCurrencyListFromDataBase() {
        for (String s : client.getRegistretedCurrency()) {
            currencyListFromDataBase.add(s);
            System.out.println("имя валюты: " + s);
        }
        logger.info("Данные с таблицы-справочника успешно получены");
    }

    public void getValueFromJournal() {
        for (FullCurValueDTO f : client.postJournalDataBaseToAnalytic()) {
            valueListFromDataBase.add(f);
            System.out.println(f);
        }
        logger.info("Данные с журнала по всем валютам успешно получены");
    }


    public void start() {

        if (isAnalyticsRunning) {
            logger.warn("Аналитик уже запущен");
            return;
        }

        Thread analyticsThread = new Thread(() -> {
            while (true) {
                try {
                    DataAnalysisByPeriod d = new DataAnalysisByPeriod();
                    finalStorage = d.analyticValue(Period.HALF_AN_HOUR, dataStorage.get());
                    Thread.sleep(20000);
                } catch (Exception e) {
                    logger.error("Ошибка в расчете показателей: " + e.getMessage());
                }

            }
        });
        analyticsThread.start();
        isAnalyticsRunning = true;
        logger.info("Аналитик запущен " + LocalDateTime.now());
    }

    public void saveList(CurrencyValueDTO dto) {
        dataStorage.put(dto);
    }

    public Set<Map.Entry<String, OHLC>> getOHLC() {
        return finalStorage.entrySet();
    }
}
