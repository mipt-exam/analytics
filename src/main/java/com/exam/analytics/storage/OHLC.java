package com.exam.analytics.storage;

import lombok.Getter;
import lombok.Setter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigDecimal;

@Getter
@Setter
public class OHLC {

    private static final Logger logger = LogManager.getLogger(OHLC.class);

    private BigDecimal open;
    private BigDecimal close;
    private BigDecimal high;
    private BigDecimal low;

    public OHLC(BigDecimal open, BigDecimal close, BigDecimal high, BigDecimal low) {
        this.open = open;
        this.close = close;
        this.high = high;
        this.low = low;
    }

    public BigDecimal open() {
        return open;
    }

    public void open(BigDecimal open) {
        this.open = open;
    }

    public BigDecimal high() {
        return high;
    }

    public void high(BigDecimal high) {
        this.high = high;
    }

    public BigDecimal low() {
        return low;
    }

    public void low(BigDecimal low) {
        this.low = low;
    }

    public BigDecimal close() {
        return close;
    }

    public void close(BigDecimal close) {
        this.close = close;
    }
}
