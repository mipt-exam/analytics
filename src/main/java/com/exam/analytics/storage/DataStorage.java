package com.exam.analytics.storage;

import com.exam.analytics.web.dto.CurrencyValueDTO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class DataStorage {

    private static final Logger logger = LogManager.getLogger(DataStorage.class);

    private final List<CurrencyValueDTO> dataList = new ArrayList<>();

    public void put(CurrencyValueDTO dto) {
        dataList.add(dto);
        logger.info("Список валютных значений сохранен");
    }

    public ArrayList<CurrencyValueDTO> get() {
        return (ArrayList<CurrencyValueDTO>) dataList.stream().collect(Collectors.toList());
    }

}
