package com.exam.analytics.web.dto;

import lombok.Getter;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
public class CurrencyValueDTO {
    private final LocalDateTime moment;
    private final BigDecimal value;

    public CurrencyValueDTO (LocalDateTime moment, BigDecimal value) {
        this.moment = moment;
        this.value = value;
    }
}
