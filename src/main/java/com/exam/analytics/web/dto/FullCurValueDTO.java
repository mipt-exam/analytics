package com.exam.analytics.web.dto;

import lombok.Getter;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
public class FullCurValueDTO {
    private long id;
    private final LocalDateTime moment;
    private final BigDecimal value;

    public FullCurValueDTO (long id, LocalDateTime moment, BigDecimal value) {
        this.id = id;
        this.moment = moment;
        this.value = value;
    }
}