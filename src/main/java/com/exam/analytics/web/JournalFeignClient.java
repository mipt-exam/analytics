package com.exam.analytics.web;

import com.exam.analytics.web.dto.CurrencyValueDTO;
import com.exam.analytics.web.dto.FullCurValueDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(name = "journal-client", url = "${journal}")
public interface JournalFeignClient {

    @GetMapping("/postReqistretedCurrencyToAnalytic")
    List<String> getRegistretedCurrency();

    @GetMapping("/postJournalDataBaseToAnalytic")
    Iterable<FullCurValueDTO> postJournalDataBaseToAnalytic();

    @GetMapping("/journal/values/period")
    List<CurrencyValueDTO> getAllByNameAndPeriod(@RequestParam String pairName,
                                                 @RequestParam String from,
                                                 @RequestParam String to);

}
