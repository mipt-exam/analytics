package com.exam.analytics.web;

import com.exam.analytics.service.AnalyticsService;
import com.exam.analytics.storage.OHLC;
import com.exam.analytics.web.dto.CurrencyValueDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
import java.util.Set;

@RestController
@RequestMapping("/api/v1")
public class AnalyticsController {

    private final JournalFeignClient client;
    private final AnalyticsService service;

    @Autowired
    public AnalyticsController(JournalFeignClient client,
                               AnalyticsService service) {
        this.client= client;
        this.service = service;
    }

    @GetMapping("/journal/values/period")
    void getAllByNameAndPeriod(@RequestParam String pairName,
                                                        @RequestParam String from,
                                                        @RequestParam String to) {

        for (CurrencyValueDTO c : client.getAllByNameAndPeriod(pairName, from, to)) {
            service.saveList(c);
        }
    }

    @GetMapping("/getOHLC")
    Set<Map.Entry<String, OHLC>> getOHLC(@RequestParam String pairName) {
        return service.getOHLC();
    }

}
